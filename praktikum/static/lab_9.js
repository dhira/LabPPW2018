var category = "quilting"
var total = 0;
$(document).ready(function() {

    $("#cat1").on({
        click: function(){
            var category =  $(this).text();
            total = 0;
            var count = document.getElementById("total").innerHTML = total;
            $("tbody").remove();
            $.ajax({
                url: "books/" + category,
                datatype: 'json',
                success: function(books){
                    var obj = jQuery.parseJSON(books)
                    renderHTML(obj);
                },
                error: function(error){
                    alert("Books not found");
                },
            })
        }
     
    })
    $("#cat2").on({
        click: function(){
            var category =  $(this).text();
            total = 0;
            var count = document.getElementById("total").innerHTML = total;
            $("tbody").remove();
            $.ajax({
                url: "books/" + category,
                datatype: 'json',
                success: function(books){
                    var obj = jQuery.parseJSON(books)
                    renderHTML(obj);
                },
                error: function(error){
                    alert("Books not found");
                },
            })
        }
     
    })
    $("#cat3").on({
        click: function(){
            var category =  $(this).text();
            total = 0;
            var count = document.getElementById("total").innerHTML = total;
            $("tbody").remove();
            $.ajax({
                url: "books/" + category,
                datatype: 'json',
                success: function(books){
                    var obj = jQuery.parseJSON(books)
                    renderHTML(obj);
                },
                error: function(error){
                    alert("Books not found");
                },
            })
        }
     
    })
	$.ajax({
		url: "books/" + category,
		datatype: 'json',
		success: function(books){
			var obj = jQuery.parseJSON(books)
			renderHTML(obj);
		}
	})
});
	
var container = document.getElementById("demo");
function renderHTML(books){
    htmlstring = "<tbody>";
    
	for(i = 0; i < books.items.length;i++){
		htmlstring += "<tr>"+
		"<th scope='row' class='align-middle text-center'style='background-color: #FFFFFF' >" + (i+1) + "</th>" +
		"<td style='background-color: #F0FFCE'><img class='img-fluid' style='width:22vh' src='" + books.items[i].volumeInfo.imageLinks.smallThumbnail +"'></img>" + "</td>" + 
		"<td class='align-middle' style='background-color: #FFFFFF'>" + books.items[i].volumeInfo.title +"</td>" +
		"<td class='align-middle' style='background-color: #F0FFCE'>" + books.items[i].volumeInfo.authors + "</td>" + 
		"<td class='align-middle' style='background-color: #FFFFFF'>" + books.items[i].volumeInfo.publisher +"</td>" + 
		"<td class='align-middle' style='background-color: #F0FFCE'>" + books.items[i].volumeInfo.pageCount +"</td>" + 
		"<td class='align-middle' style='text-align:center; background-color: #FFFFFF'>" + "<img id='bintang" + i + "' onclick='favorite(this.id)' width='28px' src='https://image.flaticon.com/icons/svg/149/149145.svg'>" + "</td></tr>";
	}   
	container.insertAdjacentHTML('beforeend', htmlstring+"</tbody>")
}

var total = 0;
function favorite(clicked_id){
	var button = document.getElementById(clicked_id);
	if(button.classList.contains("checked")){
		button.classList.remove("checked");
		document.getElementById(clicked_id).src = 'https://image.flaticon.com/icons/svg/149/149145.svg';
		total--;
		var count = document.getElementById("total").innerHTML = total;
	}
	else{
		button.classList.add('checked');
		document.getElementById(clicked_id).src = 'https://image.flaticon.com/icons/png/512/148/148764.png';
		total++;
		var count = document.getElementById("total").innerHTML = total;
	}
}
