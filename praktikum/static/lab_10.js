$(document).ready(function () {
    $('#submit_subscribe').prop('disabled', true);
    $('#form_name').hide();
    $('#form_email').hide();
    $('#form_email2').hide();
    $('#form_password').hide();
    $('#name').val("");
    $('#email').val("");
    $('#password').val("");
    
    var flag = [false, false, false, false] //index 0 untuk name, index 1 untuk email, index 2 untuk password, dan index 3 untuk email exist
    $('#name').on('input', function () {
        var input = $(this); //ambil elemen dengan id name
        check(input, 0);
        checkButton();
    });

    var timer = null;
    $('#email').keydown(function () {
        clearTimeout(timer);
        timer = setTimeout(function () {
            var input = $("#email"); //ambil elemen dengan id email
            check(input, 1);
            checkButton();
        }, 1000)
    });

    $('#password').on('input', function () {
        var input = $(this); //ambil elemen dengan id password
        check(input, 2);
        checkButton();
    });

    var check = function (input, arr) {
        if (arr === 1) { //jika email
            var reg = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/; //cek bentuk email
            var is_data = reg.test(input.val()); // jika benar bentuk email maka is_data bernilai true
            if (is_data) {
                $('#form_email').hide();
                checkEmail(input.val()); // ambil isi data formnya
                flag[arr] = true;
                return
            } else {
                $('#form_email2').hide();
            }
        } else {
            var is_data = input.val();
        }
        if (is_data) { //kalau datanya ada
            if (arr === 0) {
                $('#form_name').hide();
            } else if (arr === 1) {
                $('#form_email').hide();
            } else {
                $('#form_password').hide();
            }
            flag[arr] = true;
        } else {
            if (arr === 0) {
                $('#form_name').show();
            } else if (arr === 1) {
                $('#form_email').show();
            } else {
                $('#form_password').show();
            }
            flag[arr] = false;
        }
    };

    var checkEmail = function (email) {
        var csrftoken = $("[name=csrfmiddlewaretoken]").val();
        $.ajax({
            method: "POST",
            url: "check-email/",
            headers: {
                "X-CSRFToken": csrftoken
            },
            data: {email: email},
            success: function (response) { //response berupa boolean is_email_already_exist
                if (response.is_email) {
                    $('#form_email2').show();
                    flag[3] = false;
                    checkButton();
                } else {
                    $('#form_email2').hide();
                    flag[3] = true;
                    checkButton();
                }
            },
            error: function (error) {
                alert("Error, cannot get data from server")
            }
        })
    };

    var checkButton = function () {
        var button_subscribe = $('#submit_subscribe');
        for (var x = 0; x < flag.length; x++) {
            if (flag[x] === false) {
                button_subscribe.prop('disabled', true);
                return
            }
        }
        button_subscribe.prop('disabled', false);
    };

    $(function () {
        $('form').on('submit', function (e) {
            e.preventDefault(); //biar ga reload
            $.ajax({
                method: "POST",
                url: '',
                data: $('form').serialize(),
                success: function (status) {
                    if (status) {
                        alert("Thank you for subscribing! Enjoy my website :)");
                    } else {
                        alert("Sorry, something went wrong! Please, try again :(");
                    }
                    for (var i = 0; i < flag.length; i++) {
                        flag[i] = false
                    }
                    $("#submit_subscribe").prop('disabled', true);
                    $('#name').val("");
                    $('#email').val("");
                    $('#password').val("");
                },
                error: function (error) {
                    alert("Error, cannot connect to server")
                }
            });
        });
    });
});
