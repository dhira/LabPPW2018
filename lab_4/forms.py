from django import forms
class schedule_form(forms.Form):
    invalid_message={
       'required' : 'Please fill out this field',
       'invalid' : 'Please fill out with valid input'
    }
    date_attrs={
       'type' : 'date',
       'class' : 'todo-form-input',
    }
    time_attrs={
       'type' : 'time',
       
    }
    place_attrs={
        'type' : 'text',
        'class' : 'todo-form-input',
        'placeholder' : 'Input place',
    }
    event_attrs={
        'type' : 'text',
        'class' : 'todo-form-input',
        'placeholder' : 'Input name',
    }
    choice =(
        ('....','....'),
        ('Entertainment','Entertainment'),
        ('Sport','Sport'),
        ('Education','Education'),
        ('Art','Art'),
        ('Work','Work'),
        ('Others','Others')
    )
    choice2 =(
        ('....','....'),
        ('Monday','Monday'),
        ('Tuesday','Tuesday'),
        ('Wednesday','Wednesday'),
        ('Thursday','Thursday'),
        ('Friday','Friday'),
        ('Saturday','Saturday'),
        ('Sunday','Sunday')
    )


    event_day = forms.ChoiceField(label='Day', required=True,
       choices=choice2)
    event_date = forms.DateField(label = 'Date',required=True,
       widget=forms.DateInput(attrs=date_attrs))
    event_time = forms.TimeField(label = 'Time', required=True,
       widget=forms.TimeInput(attrs=time_attrs))
    event_name = forms.CharField(label = 'Event Name', widget=forms.TextInput(attrs=event_attrs),
       required=True,max_length=25)
    event_place = forms.CharField(label = 'Place',required=True,
       widget=forms.TextInput(attrs=place_attrs), max_length=25)
    event_category = forms.ChoiceField(label = 'Category',required=False,
      choices=choice)