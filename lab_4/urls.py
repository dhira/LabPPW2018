from django.urls import path

from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('mode/', views.mode, name='mode'),
    path('personal/', views.personal, name='personal'),
    path('business/', views.business, name='business'),
    path('cv/', views.cv, name='cv'),
    path('book/', views.book, name='book'),
    path('schedule/', views.schedule, name= 'schedule'),
    path('makeschedule/', views.makeschedule, name= 'makeschedule'),
    path('delete/', views.delete, name= 'delete'),


]
