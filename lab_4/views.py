from django.shortcuts import render
from .forms import schedule_form
from django.http import HttpResponseRedirect
from .models import Schedule
response={}

def home(request):
    return render(request, 'homepage.html', {})
def mode(request):
    return render(request, 'ModePage.html', {})
def personal(request):
    return render(request, 'Profile1.html', {})
def business(request):
    return render(request, 'Profile2.html', {})
def cv(request):
    return render(request, 'CV.html', {})
def book(request):
    return render(request, 'bukuTamu.html', {})
def delete(request):
    schedules= Schedule.objects.all().delete()

    return render(request, 'schedule.html', {})
def schedule(request):
    form = schedule_form (request.POST or None)
    if(request.method == "POST" and form.is_valid()):
        response['event_day'] = request.POST.get('event_day')
        response['event_time'] = request.POST.get('event_time')
        response['event_date'] = request.POST.get('event_date')
        response['event_name'] = request.POST.get('event_name')
        response['event_place'] = request.POST.get('event_place')
        response['event_category'] = request.POST.get('event_category')
        sched = Schedule( event_day=response['event_day'], event_date=response['event_date'], event_time=response['event_time'], event_name=response['event_name'], event_place=response['event_place'], event_category=response['event_category'])
        sched.save()
        schedules = Schedule.objects.all()
        response['schedules'] = schedules
        return render(request, "schedule.html", response)
    else:
        schedules = Schedule.objects.all()
        response['schedules'] = schedules
        return render(request, "schedule.html", response)
    
def makeschedule(request):
    response['form_make'] = schedule_form
    return render(request, 'makeschedule.html', response)

# Create your views here.
 