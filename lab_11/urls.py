from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth import views
from .views import index
from django.urls import path
from django.conf import settings



urlpatterns = [
    path('', index, name='index'),
    path('login/', views.LoginView.as_view(), name='login'),
    path('logout/',  views.LogoutView.as_view(), {'next_page': settings.LOGOUT_REDIRECT_URL}, name='logout'),
    path('auth/', include('social_django.urls', namespace='social')),
   
]