from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import *
import unittest


# Create your tests here.
class lab11UnitTest(TestCase):
    def test_lab11_url_exist(self):
        response = Client().get('/lab_11/login/')
        self.assertEqual(response.status_code, 200)

    def test_lab11_url_logout_exist(self):
        response = Client().get('/lab_11/logout/')
        self.assertEqual(response.status_code, 200)
