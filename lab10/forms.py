from django import forms


class FormSubscriber(forms.Form):
    name = forms.CharField(required=True, max_length=30, widget=forms.TextInput(attrs={'label': 'Name', 'id' : 'name',   'placeholder' : 'Input your name',}))
    email = forms.CharField(required=True, max_length=30, widget=forms.TextInput(attrs={'type': 'email',   'placeholder' : 'name@gmail.com','label': 'Email', 'id' : 'email'}))
    password = forms.CharField(required=True, min_length=8, max_length=18, widget=forms.TextInput(attrs={'type': 'password', 'label': 'Password', 'id' : 'password','placeholder' : 'min. 8 characters',}))

