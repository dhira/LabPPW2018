from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from django.test import LiveServerTestCase
from django.db import IntegrityError
from .views import subscribe, validation
from .models import Subscriber
from .forms import FormSubscriber
import unittest

# Create your tests here.
class Lab10_TestCase(TestCase):
    def test_lab10_url_is_exist(self):
        response = Client().get('/lab10/')
        self.assertEqual(response.status_code, 200)

    def test_lab10_using_subscribe_template(self):
        response = Client().get('/lab10/')
        self.assertTemplateUsed(response, 'subscribe.html')

    def test_lab10_using_subscribe_func(self):
        found = resolve('/lab10/')
        self.assertEqual(found.func, subscribe)

    def test_lab10_using_validation_func(self):
        found = resolve('/lab10/validation/')
        self.assertEqual(found.func, validation)

    def test_model_can_create_new_subscriber(self):
        new_subscriber = Subscriber.objects.create(name="didi", email="didi@gmail.com", password="dididididi")
        counting_all_subscriber = Subscriber.objects.all().count()
        self.assertEqual(counting_all_subscriber, 1)

    def test_FormSubscriber_valid(self):
        form = FormSubscriber(data={'name': "nayana", 'email': "nayana@user.com" , 'password': "123456789"})
        self.assertTrue(form.is_valid())
    
    def test_max_length_name(self):
        name = Subscriber.objects.create(name="lalalalalalalalalalalalalalala")
        self.assertLessEqual(len(str(name)), 30)
    
    def test_unique_email(self):
        Subscriber.objects.create(email="didi@email.com")
        with self.assertRaises(IntegrityError):
            Subscriber.objects.create(email="didi@email.com")
        
    def test_check_email_view_get_return_200(self):
        email = "didi@gmail.com"
        Client().post('/lab10/validation/', {'email': email})
        response = Client().post('/lab10/', {'email': 'didi@gmail.com'})
        self.assertEqual(response.status_code, 200)

    def test_check_email_already_exist_view_get_return_200(self):
        Subscriber.objects.create(name="nana", email="nana@gmail.com", password="abcdeefghi")
        response = Client().post('/lab10/validation/', {
            "email": "nana@gmail.com"
        })
        self.assertEqual(response.json()['is_email'], True)

    def test_subscribe_should_return_status_subscribe_true(self):
        response = Client().post('/lab10/', {
            "name": "bismillah",
            "email": "nana@gmail.com",
            "password":  "password",
        })
        self.assertEqual(response.json()['status_subscribe'], True)

    def test_subscribe_should_return_status_subscribe_false(self):
        name, email, password = "didi", "didi@gmail.com", "password"
        Subscriber.objects.create(name=name, email=email, password=password)
        response = Client().post('/lab10/', {
            "name": name,
            "email": email,
            "password":  password,
        })
        self.assertEqual(response.json()['status_subscribe'], False)

