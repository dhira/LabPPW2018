from django.http import JsonResponse
from django.http import HttpResponse
from django.shortcuts import render, redirect
from .models import Subscriber
from .forms import FormSubscriber
from django.views.decorators.csrf import csrf_exempt

response = {}
def subscribe(request):
    response['activetab'] = 'subscriberpage'
    if request.method == 'POST':
        form = FormSubscriber(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            email = form.cleaned_data['email']
            password = form.cleaned_data['password']
            status_subscribe = True
            try:
                Subscriber.objects.create(name=name, email=email, password=password)
            except:
                status_subscribe = False
            return JsonResponse({'status_subscribe' : status_subscribe})
    Form = FormSubscriber()
    response['form'] = Form
    return render(request, 'subscribe.html', response)

def validation(request):
    if request.method == 'POST':
        email = request.POST['email']
        valid = Subscriber.objects.filter(pk=email).exists()
        return JsonResponse({'is_email' : valid})

def listSubscriber(request):
    all_subs = Subscriber.objects.all().values()
    subs = list(all_subs)
    return JsonResponse({'all_subs' : subs})

@csrf_exempt
def unsubscribe(request):
    if request.method == 'POST':
        email = request.POST['email']
        Subscriber.objects.get(email=email).delete()
        return redirect('subscribe')


