
from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('', views.subscribe, name='subscribe'),
    path('validation/', views.validation, name="validation"),
    path('subscriber/', views.listSubscriber, name='listSubscriber'),
    path('unsubscribe/', views.unsubscribe, name='unsubscribe'),

]
