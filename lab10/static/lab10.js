$(document).ready(function () {
    $('#submit_subscribe').prop('disabled', true);
    $('.unsubBerhasil').hide();

    var flag = [false, false, false, false];
    $('#name').on('input', function () {
        var input = $(this);
        check(input, 0);
        checkButton();
    });

    var timer = null;
    $('#email').keydown(function () {
        clearTimeout(timer);
        timer = setTimeout(function () {
            var input = $("#email");
            check(input, 1);
        }, 1000);
        checkButton();
    });


    $('#password').on('input', function () {
        var input = $(this);
        check(input, 2);
        checkButton();
    });

    var check = function (input, arr) {

        if (arr == 1) {
            var reg = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
         
            var is_el = reg.test(input.val());
            
            if (is_el) {
                flag[arr] = true;
                validation(input.val());
                return
            } else {
                $(input).parent().removeClass('alert-validate2');
                flag[arr] = false;
                checkButton();
            }
        } else {
            var is_el = input.val();
        }
        if (is_el) {
            hideValidate(input);
            flag[arr] = true;
        } else {
            flag[arr] = false;
            showValidate(input)
        }
    };

    function showValidate(input) {
        input.parent().addClass('alert-validate');
    }

    function hideValidate(input) {
        input.parent().removeClass('alert-validate');
    }

    var validation = function (email) {
        var csrftoken = $("[name=csrfmiddlewaretoken]").val();
        $.ajax({
            method: "POST",
            url: "validation/",
            headers:{
                "X-CSRFToken": csrftoken
            },
            data: {email: email},
            success: function (response) {
                var inptEmail = $("#email");
                if (response.is_email) {
                    showValidate(inptEmail);
                    inptEmail.parent().addClass('alert-validate2');
                    flag[3] = false;
                    checkButton()
                } else {
                    hideValidate(inptEmail);
                    inptEmail.parent().removeClass('alert-validate2');
                    flag[3] = true;
                    checkButton()
                }
            },
            error: function (error) {
                alert("Error, cannot get data from server")
            }
        })
    };

    var checkButton = function () {
        var bttn = $('#submit_subscribe');
        for (var x = 0; x < flag.length; x++) {
            if (flag[x] === false) {
                bttn.prop('disabled', true);
                return
            }
        }
        bttn.prop('disabled', false);
    };

    $(function () {
        var csrftoken = $("[name=csrfmiddlewaretoken]").val();
        $('form').on('submit', function (e) {
            e.preventDefault();
            $.ajax({
                method: "POST",
                url: '',
                headers:{
                    "X-CSRFToken": csrftoken
                },
                data: $('form').serialize(),
                success: function (status) {
                    if (status.status_subscribe) {
                        var title1 = "<p class='teks'><strong>Thank you </strong></p>" + "<p class='teks'><strong> '"+ $('#name').val()+"' </strong></p>" + "<p class='teks'><strong>for subscribing to my page! </strong></p>" ;
                        var link = "<a href=''>Back to Form Registration</a>";
                        $("form").remove();
                       
                    } else {
                        var title1 = "<p style='font-size: 22px'><strong>Your subscription is not successful</strong></p>";
                        var link = "<a href=''>Please, try again..</a>";
                        $("form").remove();
                      
                    }
                    $(".h1").replaceWith(title1);
                    setTimeout(function(){
                        window.location.reload(1);
                     }, 5000)
                    for (var i = 0; i < flag.length; i++) {
                        flag[i] = false
                    }
                    $("#submit_subscribe").prop('disabled', true);
                    $('#name').val("");
                    $('#email').val("");
                    $('#password').val("");
                },
                error: function(error) {
                    alert("Error, cannot connect to server")
                }
            });
        });
    });

$.ajax({
    url: "subscriber/",
    datatype: 'json',
    success: function(data){
        $('tbody').html('')
        var result ='<tr>';
        for(var i = 0; i < data.all_subs.length; i++) {
            result += "<th scope='row' class='align-middle text-center'>" + (i+1) + "</th>" +
            "<td class='align-middle text-center'>" + data.all_subs[i].name +"</td>" +
            "<td class='align-middle text-center'>" + data.all_subs[i].email +"</td>" +
            "<td class='align-middle text-center'>" + "<a " + "data-email=" + data.all_subs[i].email + " class='text-white btn btn-danger unsubscribe-button' float-right role='button' >" + "Unsubscribe" + "</a></td></tr>";
        }
        $('tbody').append(result);
    },
    error: function(error){
        alert("There's no any subcribers yet");
    }
})

$('#demo').on('click', 'td .unsubscribe-button', function() {
    var email = $(this).attr('data-email');
    unsubscribe(email);
});

});
function unsubscribe(email){
$.ajax({
    method: "POST",
    url: "unsubscribe/",
    data: {email: email},
    success: function(){
        $('.unsubBerhasil').show();
        window.location.reload();
    },
})

}
