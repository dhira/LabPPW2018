from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import yourStatus
from .models import status

# Create your views here.
response={}

def hello(request):
    make_status = yourStatus (request.POST or None)
    allMessage = status.objects.all()
    response = {
        "allMessage" : allMessage
    }
    response['make_status'] = make_status
    
    if(request.method == "POST" and make_status.is_valid()):
        
        message = request.POST.get('message')
        status.objects.create(message = message)
        return redirect('hello')
    else:
        return render(request, "WebPage.html", response)
        
def profile(request):
    return render(request, 'myProfile.html', response)
    
   
   
   

