from django import forms
class yourStatus(forms.Form):
    invalid_message={
       'required' : 'Please fill out this field',
       'invalid' : 'Please fill out with valid input'
    }
    time_attrs={
       'type' : 'time',
       
    }
    message_attrs={
        'type' : 'text',
        'class' : 'form-control',
        
    }
    message = forms.CharField(label = 'Share it with us:', widget=forms.Textarea(attrs=message_attrs),
       required=True,max_length=400)
