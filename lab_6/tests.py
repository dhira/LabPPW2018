from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import hello
from .views import profile
from .models import status
from .forms import yourStatus
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
import unittest


# Create your tests here.
class UnitTest_lab6(TestCase):

    def test_lab6_url_exist(self):
        response = Client().get('/lab_6/')
        self.assertEqual(response.status_code, 200)

    def test_greet_is_exist(self):
        request = HttpRequest()
        response = hello(request)
        html_response = response.content.decode('utf8')
        self.assertIn(  'Hello! <br> How are you today?',html_response)
    
    def test_profile_url_exist(self):
        response = Client().get('/lab_6/profile')
        self.assertEqual(response.status_code, 301)

    def test_lab6_using_form_func(self):
        found = resolve('/lab_6/')
        self.assertEqual(found.func, hello)

    def test_lab6_template(self):
        response = Client().get('/lab_6/')
        self.assertTemplateUsed(response, 'WebPage.html')

    def test_profile_template(self):
        response = Client().get('/lab_6/profile/')
        self.assertTemplateUsed(response, 'myProfile.html')
    
    def test_greetings_is_exist(self):
        request = HttpRequest()
        response = profile(request)
        html_response = response.content.decode('utf8')
        self.assertIn( ' <h2 class = "h1">Nice to meet you!</h2>',html_response)
    
    def test_name_is_exist(self):
        request = HttpRequest()
        response = profile(request)
        html_response = response.content.decode('utf8')
        self.assertIn( ' <b>Nadhira Shafa Thalia</b>',html_response)
     
    def test_dob_is_exist(self):
        request = HttpRequest()
        response = profile(request)
        html_response = response.content.decode('utf8')
        self.assertIn( ' <div >Jakarta, November 12, 1999 </div>',html_response)
    
    def test_photo_is_exist(self):
        request = HttpRequest()
        response = profile(request)
        html_response = response.content.decode('utf8')
        self.assertIn( '"https://i.pinimg.com/564x/20/05/dc/2005dc725fce5730f5e99fa3f2260784.jpg"',html_response)

    def test_can_share_status(self): 
        response = self.client.post('/lab_6/',data={'message':'hari ini saya sedang berulang tahun'}) 
        counting_all_available_status = status.objects.all().count() 
        self.assertEqual(counting_all_available_status, 1)

class FunctionalTestLab6(TestCase):

    def setUp(self):
        opsi = Options()
        opsi.add_argument('--dns-prefetch-disable')
        opsi.add_argument('--no-sandbox')
        opsi.add_argument('disable-gpu')
        opsi.add_argument('--headless')
        self.selenium = webdriver.Chrome(executable_path='./chromedriver', chrome_options=opsi) 
        super(FunctionalTestLab6, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTestLab6, self).tearDown()

    
    # def test_can_start_a_list_and_retrieve_it_later(self): 

    #     selenium = self.selenium
    #     selenium.get('http://nadhira-lab6.herokuapp.com')
    #     time.sleep(2)
    #     description = selenium.find_element_by_id('id_message')
    #     submit = selenium.find_element_by_id('submit')
    #     description.send_keys('coba coba')
    #     time.sleep(2)
    #     submit.send_keys(Keys.RETURN)
    #     time.sleep(2)
    #     self.assertIn('coba coba', selenium.page_source)

    # def test_css_style_button_in_myprofile(self):
    #     selenium = self.selenium
    #     selenium.get('http://nadhira-lab6.herokuapp.com')
    #     img = selenium.find_element_by_tag_name('button')
    #     self.assertIn("submit", img.get_attribute("type"))

    # def test_css_style_button_in_webpage(self):
    #     selenium = self.selenium
    #     selenium.get('http://nadhira-lab6.herokuapp.com')
    #     img = selenium.find_element_by_id('submit')
    #     self.assertIn("submit", img.get_attribute("type"))

    def test_title_in_profile_is_valid(self):
        selenium = self.selenium
        selenium.get('http://nadhira-lab6.herokuapp.com/lab_6/profile/')
        self.assertIn("My Profile", selenium.title)
        

    def test_title_in_webpage_is_valid(self):
        selenium = self.selenium
        selenium.get('http://nadhira-lab6.herokuapp.com/lab_6/')
        self.assertIn("Greetings!", selenium.title)
