$(document).ready(function(){
    
    $(".accordion").on("click", ".accordion-header", function() {
        $(this).toggleClass("active").next().slideToggle();
    });
    
    $("#theme-button").on({
        click: function(){
            $("body").css("background-color", "#add8e6");
            $(".h1").css("color", "#4682b4");
            $(".h3").css("color", "#4682b4");
            $(".accordion").css("background-color", "#4682b4");
            $(".accordion-header").css("color", "#81D4FA");
            $(".accordion-content").css("background", "#DBF3FA");
            $(".accordion-content").css("color", "#000000");
            $("#theme-button").text('Double click for pink!');
            $(".button").css("color", "#4682b4");
        },
        dblclick: function(){
            $("body").css("background-color", "#F7Edf0");
            $(".h1").css("color", "#F4ACB7");
            $(".h3").css("color", "#F4ACB7");
            $(".accordion").css("background-color", "#F4ACB7");
            $(".accordion-header").css("color", "#FFD9D7");
            $(".accordion-content").css("background", "#fad5d5");
            $(".accordion-content").css("color", "#000000");
            $("#theme-button").text('Make it blue!');
            $(".button").css("color", "#F4ACB7");


        },
        
    }),

      $(function() {  
        $('#clickme').click(function() {
             $.ajax({
             url:  "http://localhost:8000/static/data.json",
             dataType: 'json',
             success: function(data) {
                var items = [];
                console.log(data);
      
                $.each(data, function(key, val) {
      
                  items.push('<li id="' + key + '">' + val + '</li>');    
      
                });
                $("#result").append("<tr><th>Nama:</th><th>Merk:</th><th>harga:</th></tr>")
              
                $('<ul/>', {
                   'class': 'interest-list',
                   html: items.join('')
                }).appendTo('body');
      
             },
            statusCode: {
               404: function() {
                 alert('There was a problem with the server.  Try again soon!');
               }
             }
          });
        });
      });
    
});
