from django.db import models
from django.utils import timezone 
from datetime import datetime, date

# Create your models here.
class status(models.Model):
    time = models.DateTimeField(default=timezone.now())
    message = models.TextField(max_length = 350)
def __str__(self):
    return self.message