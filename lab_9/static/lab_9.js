var category = "quilting"
var total = 0;
$(document).ready(function() {
    checkLike();
    $("#cat1").on({
        click: function(){
            var category =  $(this).text();
            total = 0;
            var count = document.getElementById("total").innerHTML = total;
            $("tbody").remove();
            $.ajax({
                url: "books/" + category,
                datatype: 'json',
                success: function(books){
                    var obj = jQuery.parseJSON(books)
                    renderHTML(obj);
                },
                error: function(error){
                    alert("Books not found");
                },
            })
        }
     
    })
    $("#cat2").on({
        click: function(){
            var category =  $(this).text();
            total = 0;
            var count = document.getElementById("total").innerHTML = total;
            $("tbody").remove();
            $.ajax({
                url: "books/" + category,
                datatype: 'json',
                success: function(books){
                    var obj = jQuery.parseJSON(books)
                    renderHTML(obj);
                },
                error: function(error){
                    alert("Books not found");
                },
            })
        }
     
    })
    $("#cat3").on({
        click: function(){
            var category =  $(this).text();
            total = 0;
            var count = document.getElementById("total").innerHTML = total;
            $("tbody").remove();
            $.ajax({
                url: "books/" + category,
                datatype: 'json',
                success: function(books){
                    var obj = jQuery.parseJSON(books)
                    renderHTML(obj);
                },
                error: function(error){
                    alert("Books not found");
                },
            })
        }
     
    })
	$.ajax({
		url: "books/" + category,
		datatype: 'json',
		success: function(books){
			var obj = jQuery.parseJSON(books)
			renderHTML(obj);
		}
	})
});
	
var container = document.getElementById("demo");
function renderHTML(books){
    htmlstring = "<tbody>";
    
	for(i = 0; i < books.items.length;i++){
		htmlstring += "<tr>"+
		"<th scope='row' class='align-middle text-center'style='background-color: #FFFFFF' >" + (i+1) + "</th>" +
		"<td style='background-color: #F0FFCE'><img class='img-fluid' style='width:22vh' src='" + books.items[i].volumeInfo.imageLinks.smallThumbnail +"'></img>" + "</td>" + 
		"<td class='align-middle' style='background-color: #FFFFFF'>" + books.items[i].volumeInfo.title +"</td>" +
		"<td class='align-middle' style='background-color: #F0FFCE'>" + books.items[i].volumeInfo.authors + "</td>" + 
		"<td class='align-middle' style='background-color: #FFFFFF'>" + books.items[i].volumeInfo.publisher +"</td>" + 
		"<td class='align-middle' style='background-color: #F0FFCE'>" + books.items[i].volumeInfo.pageCount +"</td>" + 
		"<td class='align-middle' style='text-align:center; background-color: #FFFFFF'>" + "<img id='bintang" + i + "' onclick='favorite(this.id)' width='28px' src='https://image.flaticon.com/icons/svg/149/149145.svg'>" + "</td></tr>";
	}   
    container.insertAdjacentHTML('beforeend', htmlstring+"</tbody>")
    checkLike();
}

// var total = 0;
// function favorite(clicked_id){
// 	var button = document.getElementById(clicked_id);
// 	if(button.classList.contains("checked")){
// 		button.classList.remove("checked");
// 		document.getElementById(clicked_id).src = 'https://image.flaticon.com/icons/svg/149/149145.svg';
// 		total--;
// 		var count = document.getElementById("total").innerHTML = total;
// 	}
// 	else{
// 		button.classList.add('checked');
// 		document.getElementById(clicked_id).src = 'https://image.flaticon.com/icons/png/512/148/148764.png';
// 		total++;
// 		var count = document.getElementById("total").innerHTML = total;
// 	}
// }
var countFavs = 0;
function checkLike(){
    $.ajax({
        type: 'GET',
        url: '/lab_9/getFavorite/',
        datatype : 'json',
        success: function(data){
            for(var i=1; i <= data.message.length; i++){
                console.log(data.message[i-1]);
                var id = data.message[i-1];
                var td = document.getElementById(id);
                if(td!=null){
                    td.className = 'clicked';
                    td.src = 'https://image.flaticon.com/icons/png/512/148/148764.png';
                }
                $('#total').html(data.message.length);
            }
            // $('tbody').html(print);
        }

    });
};


function favorite(id){
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    var user = document.getElementById(id);
    var liked = 'https://image.flaticon.com/icons/png/512/148/148764.png';
    var unliked = 'https://image.flaticon.com/icons/svg/149/149145.svg';
    if(user.className == 'checked'){
        $.ajax({
            url: "/lab_9/unfavorite/",
            type: "POST",
            headers: {
                "X-CSRFToken": csrftoken,
            },
            data:{
                id: id,
            },
            success: function(result){
                countFavs = result.message;
                user.className = '';
                user.src = unliked;
                $('#total').html(countFavs);
            },
            error: function(errorMessage){
                alert("Something is wrong");
            }
        });
    }else{
        $.ajax({
            url: "/lab_9/favorite/",
            type: "POST",
            headers: {
                "X-CSRFToken": csrftoken,
            },
            data:{
                id: id,
            },
            success: function(result){
                console.log(user);
                countFavs = result.message;
                user.className = 'checked';
                user.src = liked;
                $('#total').html(countFavs);
            },
            error: function(errorMessage){
                alert("Something is wrong");
            }
        })
    }
}


