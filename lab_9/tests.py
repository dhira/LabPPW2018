from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from .views import index
import unittest


# Create your tests here.


class Lab9_UnitTest(TestCase):
    def test_lab9_use_listbooks_as_template(self):
        response = Client().get('/lab_9/')
        self.assertTemplateUsed(response, 'listbook.html')
    def test_lab9_url_exists(self):
        response = Client().get('/lab_9/')
        self.assertEqual(response.status_code, 200)

    # def test_lab9_index_header(self):
    #     request = HttpRequest()
    #     response = index(request)
    #     html_response = response.content.decode('utf8')
    #     self.assertIn('Top 10', html_response)

    def test_json(self):
        response = Client().get("/lab_9/books/quilting")
        self.assertEqual(response.status_code, 200)
    
    def test_url_add_exist(self):
        response = Client().get("/lab_9/favorite/")
        self.assertEqual(response.status_code, 200)
    
    def test_url_substract_exist(self):
        response = Client().get("/lab_9/unfavorite/")
        self.assertEqual(response.status_code, 200)


class Lab9_FunctionalTest(LiveServerTestCase):
    def setUp(self):
        opsi = Options()
        opsi.add_argument('--dns-prefetch-disable')
        opsi.add_argument('--no-sandbox')
        opsi.add_argument('--headless')
        opsi.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome(executable_path='./chromedriver', chrome_options=opsi) 
        super(Lab9_FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Lab9_FunctionalTest, self).tearDown()

 
    # def test_check_title(self):
    #     selenium = self.selenium
    #     selenium.get(self.live_server_url)
    #     self.assertIn ('Best Seller',selenium.title)

    

 