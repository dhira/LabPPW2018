from django.contrib import admin
from django.urls import path
from . import views

app_name='lab_9'
urlpatterns = [
    path('', views.index, name = 'listbooks'),
    path('books/<str:category>', views.books, name = 'books'),
    path('favorite/', views.favorite, name="favorite"), 
    path('unfavorite/', views.unfavorite, name="unfavorite"),
    path('getFavorite/', views.getFavorite, name="getFavorite"), 

]