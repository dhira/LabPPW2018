from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
from django.contrib.auth import authenticate
from django.views.decorators.csrf import csrf_exempt
import requests
import json

response={}
# Create your views here.
def books(request, category):
    json_list = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + category).json()
    json_parse = json.dumps(json_list)
    return HttpResponse(json_parse)


def index(request):
    if request.user.is_authenticated and "counter" not in request.session:
        request.session['user'] = request.user.username 
        request.session['sessionId'] = request.session.session_key
        request.session['counter'] = []
    return render(request, "listbook.html")

@csrf_exempt
def favorite(request): 
    if(request.method=="POST"):
        stars = request.session["counter"]
        if request.POST["id"] not in stars:
            stars.append(request.POST["id"])
        print(request.session['sessionId'])
        print(request.session['counter'])
        request.session['counter'] = stars
        response["message"]=len(stars)
        return JsonResponse(response)
    else:
        return HttpResponse("Method not allowed")

@csrf_exempt
def unfavorite(request):
    if(request.method == "POST"):
        stars = request.session["counter"]
        if request.POST["id"] in stars:
            stars.remove(request.POST["id"])
        print(request.session['sessionId'])
        print(request.session["counter"])
        request.session["counter"] = stars
        response["message"] = len(stars)
        return JsonResponse(response)
    else:
        return HttpResponse("Method not allowed")

def getFavorite(request):
    if request.user.is_authenticated:
        if(request.method == "GET"):
            if request.session["counter"] is not None:
                response["message"] = request.session['counter']
            else:
                response["message"] = "NOT ALLOWED"
        else:
            response["message"]=""
        return JsonResponse(response)


    
